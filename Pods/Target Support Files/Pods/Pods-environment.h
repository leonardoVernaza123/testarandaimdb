
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AFNetworking
#define COCOAPODS_POD_AVAILABLE_AFNetworking
#define COCOAPODS_VERSION_MAJOR_AFNetworking 1
#define COCOAPODS_VERSION_MINOR_AFNetworking 3
#define COCOAPODS_VERSION_PATCH_AFNetworking 4

// AFOAuth2Client
#define COCOAPODS_POD_AVAILABLE_AFOAuth2Client
#define COCOAPODS_VERSION_MAJOR_AFOAuth2Client 1
#define COCOAPODS_VERSION_MINOR_AFOAuth2Client 0
#define COCOAPODS_VERSION_PATCH_AFOAuth2Client 0

// MBProgressHUD
#define COCOAPODS_POD_AVAILABLE_MBProgressHUD
#define COCOAPODS_VERSION_MAJOR_MBProgressHUD 0
#define COCOAPODS_VERSION_MINOR_MBProgressHUD 9
#define COCOAPODS_VERSION_PATCH_MBProgressHUD 0

