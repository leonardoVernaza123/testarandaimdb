//
//  detailsController.m
//  IMDB
//
//  Created by leonardo vernaza on 3/1/15.
//  Copyright (c) 2015 Jorge Vernaza. All rights reserved.
//

#import "detailsController.h"
#import "apiLogic.h"
#import "Constantes.h"

@implementation detailsController

/**
 *  In this method send a request to get the tv serie details and is created the segmented control with the seasons.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
-(void)viewDidLoad
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[apiLogic sharedInstance] requestWithParams:GET path:[NSString stringWithFormat:apiDetails,self.id] andParams:nil completionHandler:^(BOOL success, NSDictionary *result) {
        if(success==YES)
        {
            self.currentTV = result;
            if(self.currentTV)
            {
                [self loadCast];
                self.nameLabel.text=[self.currentTV objectForKey:@"name"];
                self.title =[self.currentTV objectForKey:@"name"];
                self.genreLabel.text =@"";
                for (NSDictionary* genre in [self.currentTV objectForKey:@"genres"]) {
                    if([self.genreLabel.text isEqualToString:@""])
                    {
                        self.genreLabel.text = [genre objectForKey:@"name"];
                    }
                    else
                    {
                        self.genreLabel.text = [NSString stringWithFormat:@"%@ %@,",self.genreLabel.text, [genre objectForKey:@"name"]];
                    }
                }
                if(![self.genreLabel.text isEqualToString:@""] && [self.genreLabel.text containsString:@","]) {
                   self.genreLabel.text = [self.genreLabel.text substringToIndex:[self.genreLabel.text length]-1];
                }
                NSMutableArray* seasons = [[NSMutableArray alloc] init];
                for (int i = 1; i<= [[self.currentTV objectForKey:@"number_of_seasons"] intValue];i++) {
                    [seasons addObject:[NSString stringWithFormat:@"%d",i]];
                }
                UIScrollView *scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0,self.seasonView.frame.size.width,self.seasonView.frame.size.height)];
                scroll.contentSize = CGSizeMake(30 + seasons.count * 50, self.seasonView.frame.size.height);
                scroll.showsHorizontalScrollIndicator = YES;
               UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:seasons];
                segmentedControl.frame = CGRectMake(10, 0, seasons.count * 50, 50);
                [segmentedControl addTarget:self action:@selector(changeSeason:) forControlEvents: UIControlEventValueChanged];
                segmentedControl.selectedSegmentIndex = 0;
                [scroll addSubview:segmentedControl];
                [self.seasonView addSubview:scroll];
                [self changeSeason:segmentedControl];
            }
        }
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
}

/**
 *  Use this method to send a request to get a credits.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
-(void)loadCast
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[apiLogic sharedInstance] requestWithParams:GET path:[NSString stringWithFormat:apiCredits,self.id] andParams:nil completionHandler:^(BOOL success, NSDictionary *result) {
        if(success==YES)
        {
            self.actorLabel.text=@"";
            for (NSDictionary* actor in [result objectForKey:@"cast"]) {
                if([self.actorLabel.text isEqualToString:@""])
                {
                    self.actorLabel.text = [actor objectForKey:@"name"];
                }
                else
                {
                    self.actorLabel.text = [NSString stringWithFormat:@"%@ %@,",self.actorLabel.text, [actor objectForKey:@"name"]];
                }
            }
            if(![self.actorLabel.text isEqualToString:@""] && [self.actorLabel.text containsString:@","])
            {
                self.actorLabel.text = [self.actorLabel.text substringToIndex:[self.actorLabel.text length]-1];
            }
        }
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
}

/**
 *  Use this method to show episodes when the user change the season. 
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
-(void)changeSeason:(UISegmentedControl *)segment
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[apiLogic sharedInstance] requestWithParams:GET path:[NSString stringWithFormat:apiSeason,self.id,(long)segment.selectedSegmentIndex] andParams:nil completionHandler:^(BOOL success, NSDictionary *result) {
        if(success==YES)
        {
            self.selectedSeason = result;
            [self.episodesTableView reloadData];
        }
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
    
}

/**
 *  In this method sets the episodes rows.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.selectedSeason && [self.selectedSeason objectForKey:@"episodes"])
    {
        return [[self.selectedSeason objectForKey:@"episodes"] count];
    }
    else
    {
        return 0;
    }
}

/**
 *  This method to sets the cell of current row.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary* record = [[self.selectedSeason objectForKey:@"episodes"] objectAtIndex:indexPath.row];
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellEpisode"];
    if(record)
        {
              cell.textLabel.text = [record objectForKey:@"name"];
        }
 
    return cell;
}
@end
