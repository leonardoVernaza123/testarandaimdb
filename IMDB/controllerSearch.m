//
//  controllerSearch.m
//  IMDB
//
//  Created by Leonardo Vernaza on 3/1/15.
//  Copyright (c) 2015 Jorge Vernaza. All rights reserved.
//

#import "controllerSearch.h"
#import "collectionCell.h"
#import "apiLogic.h"
#import "Constantes.h"
#import "detailsController.h"

@implementation controllerSearch

/**
 *  In this method is initializated the oauth client and add the searchbar to collection view.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
-(void)viewDidLoad
{
    [[apiLogic sharedInstance] setupClientCredentialsWithBaseStringURL:apiUrl];
    self.title = @"IMDB";
    page = 1;
    self.searchArray = [[NSMutableArray alloc] init];
    
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.collectionView.frame), 44)];
    self.searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    self.searchBar.placeholder =@"Buscar";
    [self.searchBar setReturnKeyType:UIReturnKeySearch];
    self.searchBar.delegate = self;
    [self.collectionView addSubview:self.searchBar];
}

/**
 *  In this method send the request to get the movies and tv series with query criteria.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
-(void)search
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[apiLogic sharedInstance] requestWithParams:GET path:apiSearch andParams:@{@"query":self.searchBar.text, @"page": [NSString stringWithFormat:@"%d",page]} completionHandler:^(BOOL success, NSDictionary *result) {
        if(success == YES)
        {
            if(page == 1)
            {
                self.searchArray = [[NSMutableArray alloc] init];
                [self.searchArray addObjectsFromArray:[result objectForKey:@"results"]];
            }
            else{
                [self.searchArray addObjectsFromArray:[result objectForKey:@"results"]];
            }
        }
        else{
            self.searchArray = [[NSMutableArray alloc] init];
        }
        [self.collectionView reloadData];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
}

/**
 *  This method download the image in background to display in collection view.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, NSData *data))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (!error) {
            completionBlock(YES, data);
        } else {
            completionBlock(NO, nil);
        }
    }];
}

/**
 *  This method sets the items in the collection view.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(self.searchArray)
    {
        return self.searchArray.count;
    }
    else
    {
        return 0;
    }
}

/**
 *  In this method sets the collection view cell with the records values.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellIMDB";
    collectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];

    if(self.searchArray && self.searchArray.count >= indexPath.row +1)
    {
        NSMutableDictionary* record = [self.searchArray objectAtIndex:indexPath.row];
        if([record objectForKey:@"name"])
        {
            cell.nameMovieLabel.text =  [record objectForKey:@"name"];
        }
        else if([record objectForKey:@"title"])
        {
            cell.nameMovieLabel.text =  [record objectForKey:@"title"];
        }
        cell.imageMovieImageView.image=[UIImage imageNamed:@"imageMovie"];
        if(![record objectForKey:@"image"])
        {
            if([record objectForKey:@"poster_path"])
            {
                __weak NSIndexPath *weakRow = indexPath;
                __weak NSMutableDictionary *weakRecord = record;
                __weak collectionCell *weakCell = cell;
                [self downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",imageUrl , [record objectForKey:@"poster_path"]]] completionBlock:^(BOOL succeeded, NSData *data) {
                    if (succeeded) {
                        NSMutableDictionary* newRecord = [[NSMutableDictionary alloc] initWithDictionary:weakRecord];
                        [newRecord setObject:[[UIImage alloc] initWithData:data] forKey:@"image"];
                        [self.searchArray replaceObjectAtIndex:weakRow.row withObject:newRecord];
                        weakCell.imageMovieImageView.image =[newRecord objectForKey:@"image"];
                        newRecord=nil;
                    }
                    else
                    {
                         NSMutableDictionary* newRecord = [[NSMutableDictionary alloc] initWithDictionary:weakRecord];
                        [newRecord setObject:[UIImage imageNamed:@"imageMovie"] forKey:@"image"];
                        [self.searchArray replaceObjectAtIndex:weakRow.row withObject:newRecord];
                        weakCell.imageMovieImageView.image =[newRecord objectForKey:@"image"];
                        newRecord=nil;
                    }
                }];
            }
            else
            {
                NSMutableDictionary* newRecord = [[NSMutableDictionary alloc] initWithDictionary:record];
                [newRecord setObject:[UIImage imageNamed:@"imageMovie"] forKey:@"image"];
                [self.searchArray replaceObjectAtIndex:indexPath.row withObject:newRecord];
                cell.imageMovieImageView.image =[newRecord objectForKey:@"image"];
                newRecord=nil;
            }
        }
        else
        {
            cell.imageMovieImageView.image =[record objectForKey:@"image"];
        }
    }
    return cell;
}

/**
 *  call This method when the scroll is in end to send new request.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView;
{
    page += 1;
    [self search];
}

/**
 * Call this method when the user select a item to open details if the selected item is a movie show a alert view.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary* record = [self.searchArray objectAtIndex:indexPath.row];
    if([[record objectForKey:@"media_type"] isEqualToString:@"tv"])
        {
            detailsController *detailsController = [self.storyboard instantiateViewControllerWithIdentifier:@"detailsController"];
            detailsController.id = [record objectForKey:@"id"];
            [self.navigationController pushViewController:detailsController animated:YES];
        }
    else
    {
       UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"IMDB"
                                   message:@"El detalle solo esta disponible para las series"
                                  delegate:self
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [alert show];
    }
    
}

/**
 *  In this method when the user type a new criteria send a request .
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([searchText containsString:@"@"] || [searchText containsString:@"#"])
    {
        searchText = [searchText stringByReplacingOccurrencesOfString:@"#" withString:@""];
        searchText = [searchText stringByReplacingOccurrencesOfString:@"@" withString:@""];
        self.searchBar.text=searchText;
        return;
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    page=1;
    [self search];
}
@end
