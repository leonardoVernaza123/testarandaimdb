//
//  Constantes.h
//  IMDB
//
//  Created by leonardo vernaza on 3/1/15.
//  Copyright (c) 2015 Jorge Vernaza. All rights reserved.
//

#ifndef IMDB_Constantes_h
#define IMDB_Constantes_h


#define apiKey @"aa613ee7e4be112f87ead02d956e6b57"
#define apiUrl @"http://api.themoviedb.org"

#define imageUrl @"https://image.tmdb.org/t/p/w185/"
#define apiSearch @"/3/search/multi"
#define apiDetails @"http://api.themoviedb.org/3/tv/%@"
#define apiSeason @"http://api.themoviedb.org/3/tv/%@/season/%ld"
#define apiCredits @"http://api.themoviedb.org/3/tv/%@/credits"
#endif
