//
//  main.m
//  IMDB
//
//  Created by Jose Ortega on 3/1/15.
//  Copyright (c) 2015 Jorge Vernaza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
