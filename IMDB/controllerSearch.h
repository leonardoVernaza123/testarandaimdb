//
//  controllerSearch.h
//  IMDB
//
//  Created by Jose Ortega on 3/1/15.
//  Copyright (c) 2015 Jorge Vernaza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface controllerSearch : UICollectionViewController<UISearchBarDelegate,UITextFieldDelegate>
{
    int page;
}

/**
 *  Property that gets and sets the array from request.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
@property NSMutableArray* searchArray;

/**
 *  UISearchBar to type the search value.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
@property UISearchBar* searchBar;

@end
