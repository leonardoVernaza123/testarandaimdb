//
//  logicaApi.h
//  IMDB
//
//  Created by leonardo vernaza on 3/1/15.
//  Copyright (c) 2015 Jorge Vernaza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFOAuth2Client.h"

@interface apiLogic : NSObject

/**
 *  Use this enumeration to set the http method.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
typedef NS_ENUM(NSInteger, httpMethod) {
    GET,
    POST,
    PUT,
    DELETE
};

#pragma mark - Class Methods

/**
 *  Use this method to get an instance of this class.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 *  @return {instancetype} Return an instnace of this class.
 */
+(instancetype)sharedInstance;

#pragma mark - Properties

/**
 *  property that gets and sets the oAuth object.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
@property (strong, nonatomic) AFOAuth2Client *oauthClient;

#pragma mark - Instance Methods

/**
 *  Use this method to set the oauthClient object with url.
 *  creates an instance of the AFOAuth2Client class, the information is assigned from: url.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 *  @param {NSString} stringURL: The url of server.
 */
-(void)setupClientCredentialsWithBaseStringURL:(NSString *)stringURL;

/**
 *  Use this method to create new request
 *  With the oauthClient object creates a NSMutableURLRequest with requestWithMethod and parameters.
 *  Creates an NSURLConnection, and becomes a so-called asynchronous with the request.
 *  If the response is OK returns a dictionary with client token if the response is not OK returns a dictionary with a description of the error.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 *  @param {httpMethod} method: The method http of the endpoint.
 *  @param {NSString} path: The path of endpoint.
 *  @param {NSDictionary} params: The parameter to the backend.
 *  @param {void} completionHandler: A block which receives the results of request.
 */
- (void)requestWithParams:(httpMethod)method path:(NSString *)path andParams:(NSDictionary *)params completionHandler: (void (^)(BOOL success, NSDictionary *result))completionHandler;
@end
