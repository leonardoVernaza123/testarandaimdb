//
//  logicaApi.m
//  IMDB
//
//  Created by leonardo vernaza on 3/1/15.
//  Copyright (c) 2015 Jorge Vernaza. All rights reserved.
//

#import "apiLogic.h"
#import "Constantes.h"

@implementation apiLogic

#pragma mark - Class Methods

/**
 *  Use this method to get an instance of this class.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 *  @return {instancetype} Return an instnace of this class.
 */
+(instancetype)sharedInstance{
    static apiLogic *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc]init];
    });
    
    return instance;
}

#pragma mark - Instance Methods

/**
 *  Use this method to set the oauthClient object with url.
 *  creates an instance of the AFOAuth2Client class, the information is assigned from: url.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 *  @param {NSString} stringURL: The url of server.
 */
-(void)setupClientCredentialsWithBaseStringURL:(NSString *)stringURL
{
    NSURL *baseURL     = [NSURL URLWithString:apiUrl];
    self.oauthClient = [AFOAuth2Client clientWithBaseURL:baseURL];
}

/**
 *  Use this method to create new request
 *  With the oauthClient object creates a NSMutableURLRequest with requestWithMethod and parameters.
 *  Creates an NSURLConnection, and becomes a so-called asynchronous with the request.
 *  If the response is OK returns a dictionary with api key if the response is not OK returns a dictionary with a description of the error.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 *  @param {httpMethod} method: The method http of the endpoint.
 *  @param {NSString} path: The path of endpoint.
 *  @param {NSDictionary} params: The parameter to the backend.
 *  @param {void} completionHandler: A block which receives the results of request.
 */
- (void)requestWithParams:(httpMethod)method path:(NSString *)path andParams:(NSDictionary *)params completionHandler: (void (^)(BOOL success, NSDictionary *result))completionHandler
{
    @try {
        NSMutableURLRequest *request;
        NSMutableDictionary *paramsWithToken = [[NSMutableDictionary alloc] initWithDictionary:params];
        [paramsWithToken setValue:apiKey forKey:@"api_key"];
        request=[self.oauthClient requestWithMethod:[self getStringHttpMethod:method] path:path parameters:paramsWithToken];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                                   NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
                                   if([httpResponse statusCode]==200)
                                   {
                                       //SUCCESS
                                       if(data!=nil && data.length>0)
                                       {
                                           NSDictionary *success= [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
                                            completionHandler(YES, success);
                                       }
                                       else
                                       {
                                           completionHandler(NO, @{@"message": @"An error ocurred"});
                                       }
                                   }
                                   else
                                   {
                                       completionHandler(NO, @{@"message": @"An error ocurred"});
                                   }
                               }];
    }
    @catch (NSException *exception) {
        completionHandler(NO, @{@"message": @"An error ocurred"});
    }
}

/**
 * Use this function to get the string of the type method.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 *  @param {httpMethod} method: The http method from enumeration.
 *  @return {NSString} Return the string to http method.
 */
-(NSString *)getStringHttpMethod:(httpMethod)method
{
    switch (method) {
        case GET:
            return @"GET";
            break;
        case POST:
            return @"POST";
            break;
        case PUT:
            return @"PUT";
            break;
        case DELETE:
            return @"DELETE";
            break;
        default:
            return @"";
            break;
    }
}

@end
