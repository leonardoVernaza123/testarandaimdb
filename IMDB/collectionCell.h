//
//  collectionCell.h
//  IMDB
//
//  Created by Leonardo Vernaza on 3/1/15.
//  Copyright (c) 2015 Jorge Vernaza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface collectionCell : UICollectionViewCell

/**
 *  Label to display the movie name.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
@property (weak,nonatomic) IBOutlet UILabel* nameMovieLabel;

/**
 *  ImageView to display the movie image
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
@property (weak,nonatomic) IBOutlet UIImageView* imageMovieImageView;

@end
