//
//  detailsController.h
//  IMDB
//
//  Created by leonardo vernaza on 3/1/15.
//  Copyright (c) 2015 Jorge Vernaza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface detailsController : UIViewController<UITableViewDataSource,UITableViewDelegate>

/**
 *  Property that sets and gets the id of tv serie.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
@property NSString* id;

/**
 *  Property that sets and gets the current tv serie dictionary.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
@property NSDictionary* currentTV;

/**
 *  Property that sets and gets the selected season.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
@property NSDictionary* selectedSeason;

/**
 *  Label to display the tv serie name.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
@property (weak,nonatomic) IBOutlet UILabel* nameLabel;

/**
 *  Label to display the tv serie genre.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
@property (weak,nonatomic) IBOutlet UILabel* genreLabel;

/**
 *  Label to display the the actors.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
@property (weak,nonatomic) IBOutlet UILabel* actorLabel;

/**
 *  view to display seasons.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
@property (weak,nonatomic) IBOutlet UIView* seasonView;

/**
 *  Table view to show the episodes.
 *  @author Leonardo Vernaza <leo0307vb@hotmail.com>.
 */
@property (weak,nonatomic) IBOutlet UITableView* episodesTableView;

@end
